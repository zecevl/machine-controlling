package raf.rs.NWP3.services;

import java.util.List;
import java.util.Optional;

public interface IService<T, ID> {
    <S extends T> S save(S var1);

    Optional<T> findById(ID var1);

    void deleteById(ID var1);

    List<T> findAll();

    Optional<T> findByEmail(String var1);


}

