package raf.rs.NWP3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import raf.rs.NWP3.forms.ActionForm;
import raf.rs.NWP3.forms.CreateMachineForm;
import raf.rs.NWP3.forms.SearchForm;
import raf.rs.NWP3.model.*;
import raf.rs.NWP3.repositories.ErrorMessageRepository;
import raf.rs.NWP3.repositories.MachineRepository;
import raf.rs.NWP3.services.MachineService;
import raf.rs.NWP3.services.UserService;
import raf.rs.NWP3.util.JwtUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/machine")
public class MachineController {

    private MachineService machineService;
    private JwtUtil jwtUtil;
    private UserService userService;
    private MachineRepository machineRepository;
    private final ErrorMessageRepository errorMessageRepository;

    @Autowired
    public MachineController(MachineService machineService, JwtUtil jwtUtil, UserService userService, MachineRepository machineRepository, ErrorMessageRepository errorMessageRepository) {
        this.machineService = machineService;
        this.jwtUtil = jwtUtil;
        this.userService = userService;
        this.machineRepository = machineRepository;
        this.errorMessageRepository = errorMessageRepository;
    }



    @PostMapping(value = "/search", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Machines> searchMachines(@RequestHeader("Authorization") String token, @RequestBody SearchForm searchForm){
        UserDetails user = this.checkJwt(token);
        List<Machine> toRet = new ArrayList<>();
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Optional<User> optionalUser = userService.findByEmail(user.getUsername());
        if(optionalUser.isPresent()) {
            if (machineService.findByCreatedBy(optionalUser.get()).isPresent() && optionalUser.get().isCan_search_machines()) {

                if (machineService.findByCreatedBy(optionalUser.get()).get().isEmpty())
                    return new ResponseEntity<>(new Machines(toRet), HttpStatus.ACCEPTED);

                if (searchForm.getName() != null && searchForm.getStatus() != null && searchForm.getDateFrom() != null && searchForm.getDateTo() != null){
                    if (machineRepository.findByCreatedByAndNameContainingAndStatusAndDateBetweenAndActiveTrue(optionalUser.get(),searchForm.getName(), Status.valueOf(searchForm.getStatus()) ,searchForm.getDateFrom(),searchForm.getDateTo()).isPresent()){
                        toRet = machineRepository.findByCreatedByAndNameContainingAndStatusAndDateBetweenAndActiveTrue(optionalUser.get(),searchForm.getName(), Status.valueOf(searchForm.getStatus()) ,searchForm.getDateFrom(),searchForm.getDateTo()).get();
                    }
                }
                else if (searchForm.getName() != null && searchForm.getStatus() != null){
                    if (machineRepository.findByCreatedByAndNameContainingAndStatusAndActiveTrue(optionalUser.get(),searchForm.getName(), Status.valueOf(searchForm.getStatus())).isPresent()){
                        toRet = machineRepository.findByCreatedByAndNameContainingAndStatusAndActiveTrue(optionalUser.get(),searchForm.getName(), Status.valueOf(searchForm.getStatus())).get();
                    }
                }
                else if (searchForm.getStatus() != null && searchForm.getDateFrom() != null && searchForm.getDateTo() != null){
                    if (machineRepository.findByCreatedByAndStatusAndDateBetweenAndActiveTrue(optionalUser.get(), Status.valueOf(searchForm.getStatus()) ,searchForm.getDateFrom(),searchForm.getDateTo()).isPresent()){
                        toRet = machineRepository.findByCreatedByAndStatusAndDateBetweenAndActiveTrue(optionalUser.get(), Status.valueOf(searchForm.getStatus()) ,searchForm.getDateFrom(),searchForm.getDateTo()).get();
                    }
                }
                else if (searchForm.getName() != null  && searchForm.getDateFrom() != null && searchForm.getDateTo() != null){
                    if (machineRepository.findByCreatedByAndNameContainingAndDateBetweenAndActiveTrue(optionalUser.get(),searchForm.getName() ,searchForm.getDateFrom(),searchForm.getDateTo()).isPresent()){
                        toRet = machineRepository.findByCreatedByAndNameContainingAndDateBetweenAndActiveTrue(optionalUser.get(),searchForm.getName() ,searchForm.getDateFrom(),searchForm.getDateTo()).get();
                    }
                }
                else if (searchForm.getName() != null){
                    if (machineRepository.findByCreatedByAndNameContainingAndActiveTrue(optionalUser.get(),searchForm.getName()).isPresent()){
                        toRet = machineRepository.findByCreatedByAndNameContainingAndActiveTrue(optionalUser.get(),searchForm.getName()).get();
                    }
                }
                else if (searchForm.getStatus() != null){
                    if (machineRepository.findByCreatedByAndStatusAndActiveTrue(optionalUser.get(), Status.valueOf(searchForm.getStatus())).isPresent()){
                        toRet = machineRepository.findByCreatedByAndStatusAndActiveTrue(optionalUser.get(), Status.valueOf(searchForm.getStatus())).get();
                    }
                }
                else if (searchForm.getDateFrom() != null && searchForm.getDateTo() != null){
                    if (machineRepository.findByCreatedByAndDateBetweenAndActiveTrue(optionalUser.get(),searchForm.getDateFrom(),searchForm.getDateTo()).isPresent()){
                        toRet = machineRepository.findByCreatedByAndDateBetweenAndActiveTrue(optionalUser.get() ,searchForm.getDateFrom(),searchForm.getDateTo()).get();
                    }
                }
                else{
                    if (machineRepository.findByCreatedByAndActiveTrue(optionalUser.get()).isPresent()){
                        toRet = machineRepository.findByCreatedByAndActiveTrue(optionalUser.get()).get();
                    }
                }

            }else{
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
            return new ResponseEntity<>(new Machines(toRet), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Machine> createMachine(@RequestHeader("Authorization") String token, @RequestBody CreateMachineForm createMachineForm) {

        UserDetails userDetails = this.checkJwt(token);
        if (userDetails == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: userDetails.getAuthorities()) {
            if (obj.getAuthority().equals("can_create_machine")) {


                Optional<User> optionalUser = userService.findByEmail(userDetails.getUsername());


                Machine machine = new Machine();
                machine.setName(createMachineForm.getName());
                machine.setActive(true);
                machine.setCreatedBy(optionalUser.get());
                machine.setStatus(Status.STOPPED);
                //optionalUser.get().getMachineList().add(machine);

                return new ResponseEntity<>(machineService.save(machine), HttpStatus.ACCEPTED);
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Machine> readMachine(@RequestHeader("Authorization") String token, @PathVariable long id){
        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);

        Optional<Machine> optionalMachine = machineService.findById(id);
        Optional<List<Machine>> optionalMachines = machineService.findByCreatedBy(userService.findByEmail(user.getUsername()).get());
        if(optionalMachine.isPresent() && optionalMachines.get().contains(optionalMachine.get())) {
            return new ResponseEntity<>(optionalMachine.get(),HttpStatus.ACCEPTED);
        }else{
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Machine> deleteMachine(@RequestHeader("Authorization") String token, @PathVariable Long id) {

        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: user.getAuthorities()) {
            if (obj.getAuthority().equals("can_destroy_machine")) {

                Optional<Machine> optionalMachine = machineService.findById(id);
                Optional<List<Machine>> optionalMachines = machineService.findByCreatedBy(userService.findByEmail(user.getUsername()).get());
                if(optionalMachine.isPresent() && optionalMachines.get().contains(optionalMachine.get())) {
                    if (optionalMachine.get().getStatus()== Status.STOPPED) {
                        optionalMachine.get().setActive(false);
                        machineService.save(optionalMachine.get());
                        return new ResponseEntity<>(optionalMachine.get(), HttpStatus.ACCEPTED);
                    }
                    else{
                        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                    }
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);

    }

    @PostMapping(value = "/start/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> startMachine(@RequestHeader("Authorization") String token, @RequestBody ActionForm actionForm, @PathVariable Long id) {

        UserDetails userDetails = this.checkJwt(token);
        if (userDetails == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: userDetails.getAuthorities()) {
            if (obj.getAuthority().equals("can_start_machine")) {

                Optional<Machine> optionalMachine = machineRepository.findById(id);
                if (optionalMachine.isPresent()){
                    if (optionalMachine.get().getStatus() == Status.STOPPED){
                        String message = machineService.startMachine(optionalMachine.get(),actionForm);
                        if (message.equals("")){
                            return new ResponseEntity<>("",HttpStatus.ACCEPTED);
                        }else{
                            ErrorMessages errorMessages = new ErrorMessages(message,getToday(),userService.findByEmail(userDetails.getUsername()).get(),optionalMachine.get());
                            errorMessageRepository.saveAndFlush(errorMessages);
                            return new ResponseEntity<>(message,HttpStatus.BAD_REQUEST);
                        }
                    }
                    else{
                        ErrorMessages errorMessages = new ErrorMessages("Machine is already running",getToday(),userService.findByEmail(userDetails.getUsername()).get(),optionalMachine.get());
                        errorMessageRepository.saveAndFlush(errorMessages);
                        return new ResponseEntity<>("Machine is already running",HttpStatus.BAD_REQUEST);
                    }
                }else{
                    ErrorMessages errorMessages = new ErrorMessages("Machine not found",getToday(),userService.findByEmail(userDetails.getUsername()).get(),null);
                    errorMessageRepository.saveAndFlush(errorMessages);
                    return new ResponseEntity<>("Machine not found",HttpStatus.NOT_FOUND);
                }


            }
        }
        ErrorMessages errorMessages = new ErrorMessages("No permission",getToday(),userService.findByEmail(userDetails.getUsername()).get(),null);
        errorMessageRepository.saveAndFlush(errorMessages);
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PostMapping(value = "/stop/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> stopMachine(@RequestHeader("Authorization") String token, @RequestBody ActionForm actionForm, @PathVariable Long id) {

        UserDetails userDetails = this.checkJwt(token);
        if (userDetails == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: userDetails.getAuthorities()) {
            if (obj.getAuthority().equals("can_stop_machine")) {

                Optional<Machine> optionalMachine = machineRepository.findById(id);
                if (optionalMachine.isPresent()){
                    if (optionalMachine.get().getStatus() == Status.RUNNING){
                        String message = machineService.stopMachine(optionalMachine.get(),actionForm);
                        if (message.equals("")){
                            return new ResponseEntity<>("",HttpStatus.ACCEPTED);
                        }else{
                            ErrorMessages errorMessages = new ErrorMessages(message,getToday(),userService.findByEmail(userDetails.getUsername()).get(),optionalMachine.get());
                            errorMessageRepository.saveAndFlush(errorMessages);
                            return new ResponseEntity<>(message,HttpStatus.BAD_REQUEST);
                        }
                    }
                    else{
                        ErrorMessages errorMessages = new ErrorMessages("Machine is not running",getToday(),userService.findByEmail(userDetails.getUsername()).get(),optionalMachine.get());
                        errorMessageRepository.saveAndFlush(errorMessages);
                        return new ResponseEntity<>("Machine is not running",HttpStatus.BAD_REQUEST);
                    }
                }else{
                    ErrorMessages errorMessages = new ErrorMessages("Machine not found",getToday(),userService.findByEmail(userDetails.getUsername()).get(),null);
                    errorMessageRepository.saveAndFlush(errorMessages);
                    return new ResponseEntity<>("Machine not found",HttpStatus.NOT_FOUND);
                }


            }
        }
        ErrorMessages errorMessages = new ErrorMessages("No permission",getToday(),userService.findByEmail(userDetails.getUsername()).get(),null);
        errorMessageRepository.saveAndFlush(errorMessages);
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PostMapping(value = "/restart/{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> restartMachine(@RequestHeader("Authorization") String token, @RequestBody ActionForm actionForm, @PathVariable Long id) {

        UserDetails userDetails = this.checkJwt(token);
        if (userDetails == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: userDetails.getAuthorities()) {
            if (obj.getAuthority().equals("can_restart_machine")) {

                Optional<Machine> optionalMachine = machineRepository.findById(id);
                if (optionalMachine.isPresent()){
                    if (optionalMachine.get().getStatus() == Status.RUNNING){
                        String message = machineService.restartMachine(optionalMachine.get(),actionForm);
                        if (message.equals("")){
                            return new ResponseEntity<>("",HttpStatus.ACCEPTED);
                        }else{
                            ErrorMessages errorMessages = new ErrorMessages(message,getToday(),userService.findByEmail(userDetails.getUsername()).get(),optionalMachine.get());
                            errorMessageRepository.saveAndFlush(errorMessages);
                            return new ResponseEntity<>(message,HttpStatus.BAD_REQUEST);
                        }
                    }
                    else{
                        ErrorMessages errorMessages = new ErrorMessages("Machine is not running",getToday(),userService.findByEmail(userDetails.getUsername()).get(),optionalMachine.get());
                        errorMessageRepository.saveAndFlush(errorMessages);
                        return new ResponseEntity<>("Machine is not running",HttpStatus.BAD_REQUEST);
                    }
                }else{
                    ErrorMessages errorMessages = new ErrorMessages("Machine not found",getToday(),userService.findByEmail(userDetails.getUsername()).get(),null);
                    errorMessageRepository.saveAndFlush(errorMessages);
                    return new ResponseEntity<>("Machine not found",HttpStatus.NOT_FOUND);
                }


            }
        }
        ErrorMessages errorMessages = new ErrorMessages("No permission",getToday(),userService.findByEmail(userDetails.getUsername()).get(),null);
        errorMessageRepository.saveAndFlush(errorMessages);
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }


    private UserDetails checkJwt(String token){
        String jwt = null;
        String username = null;

        if(token != null && token.startsWith("Bearer ")) {
            jwt = token.substring(7);
            username = jwtUtil.extractUsername(jwt);
        }

        if (username != null ) {

            UserDetails userDetails = this.userService.loadUserByUsername(username);

            if (jwtUtil.validateToken(jwt, userDetails)) {

                return userDetails;
            }

        }
        return null;
    }

    private Date getToday(){
        long millis=System.currentTimeMillis();
        Date date = new Date(millis);
        date.setHours(LocalDateTime.now().getHour());
        date.setMinutes(LocalDateTime.now().getMinute());
        return date;
    }
}
