package raf.rs.NWP3.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Users {

    List<User> users;

    public Users(List<User> users) {
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
