package raf.rs.NWP3.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import raf.rs.NWP3.model.User;
import raf.rs.NWP3.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService, IService<User,Long> {

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User myUser = this.userRepository.findByEmail(username).get();
        if(myUser == null) {
            throw new UsernameNotFoundException("User name "+username+" not found");
        }

        List<GrantedAuthority> roles = new ArrayList<>();

        if (myUser.isCan_create_users()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_create";
                }
            });
        }
        if (myUser.isCan_delete_users()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_delete";
                }
            });
        }
        if (myUser.isCan_read_users()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_read";
                }
            });
        }
        if (myUser.isCan_update_users()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_update";
                }
            });
        }

        if (myUser.isCan_create_machines()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_create_machine";
                }
            });
        }
        if (myUser.isCan_destroy_machines()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_destroy_machine";
                }
            });
        }
        if (myUser.isCan_restart_machines()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_restart_machine";
                }
            });
        }
        if (myUser.isCan_search_machines()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_search_machine";
                }
            });
        }

        if (myUser.isCan_start_machines()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_start_machine";
                }
            });
        }
        if (myUser.isCan_stop_machines()){
            roles.add(new GrantedAuthority() {
                @Override
                public String getAuthority() {
                    return "can_stop_machine";
                }
            });
        }
        return new org.springframework.security.core.userdetails.User(myUser.getEmail(), myUser.getPassword(), roles);
    }

    @Override
    public <S extends User> S save(S var1) {
        return userRepository.saveAndFlush(var1);
    }

    @Override
    public Optional<User> findById(Long var1) {
        return userRepository.findById(var1);
    }

    @Override
    public void deleteById(Long var1) {
        userRepository.deleteById(var1);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public Optional<User> findByEmail(String var1) {
        return userRepository.findByEmail(var1);
    }
}
