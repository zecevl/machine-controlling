package raf.rs.NWP3.forms;

import lombok.Data;

@Data
public class JwtForm {
    private String jwt;

    public JwtForm(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}
