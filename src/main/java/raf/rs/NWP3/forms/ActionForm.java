package raf.rs.NWP3.forms;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ActionForm {

    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
