package raf.rs.NWP3.model;

import java.util.List;

public class Machines {

    List<Machine> machines;

    public Machines(List<Machine> machines) {
        this.machines = machines;
    }

    public List<Machine> getMachines() {
        return machines;
    }

    public void setMachines(List<Machine> machines) {
        this.machines = machines;
    }
}
