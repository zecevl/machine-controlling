package raf.rs.NWP3.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.ObjectOptimisticLockingFailureException;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import raf.rs.NWP3.forms.ActionForm;
import raf.rs.NWP3.model.ErrorMessages;
import raf.rs.NWP3.model.Machine;
import raf.rs.NWP3.model.Status;
import raf.rs.NWP3.model.User;
import raf.rs.NWP3.repositories.ErrorMessageRepository;
import raf.rs.NWP3.repositories.MachineRepository;
import org.springframework.scheduling.support.CronTrigger;


import java.util.Date;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class MachineService implements IService<Machine,Long>{

    private MachineRepository machineRepository;
    private TaskScheduler taskScheduler;
    private ErrorMessageRepository errorMessageRepository;

    @Autowired
    public MachineService(MachineRepository machineRepository, TaskScheduler taskScheduler, ErrorMessageRepository errorMessageRepository) {
        this.machineRepository = machineRepository;
        this.taskScheduler = taskScheduler;
        this.errorMessageRepository = errorMessageRepository;
    }


    @Override
    public <S extends Machine> S save(S var1) {
        return machineRepository.saveAndFlush(var1);
    }

    @Override
    public Optional<Machine> findById(Long var1) {
        return machineRepository.findById(var1);
    }

    @Override
    public void deleteById(Long var1) {
        machineRepository.deleteById(var1);
    }

    @Override
    public List<Machine> findAll() {
        return machineRepository.findAll();
    }

    @Override
    public Optional<Machine> findByEmail(String var1) {
        return Optional.empty();
    }

    public Optional<List<Machine>> findByCreatedBy(User var1) {
        return machineRepository.findByCreatedByAndActiveTrue(var1);
    }

    public String startMachine(Machine machine, ActionForm actionForm){

            Random random = new Random();

            if (actionForm.getDate()!=null) {

                Date date = actionForm.getDate();

                CronTrigger cronTrigger = new CronTrigger(date.getMinutes() + " " + date.getHours() + " " + date.getDate() + " " + date.getMonth() + " " + date.getDay() + " "  + date.getYear());

                //<Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week>
               // <Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week> <Year>

                var wrapper = new Object(){ String value = ""; };
                this.taskScheduler.schedule(() -> {

                    try {
                        Thread.sleep(10000 + random.nextInt(4000));
                        machine.setStatus(Status.RUNNING);
                        machine.setDate(actionForm.getDate());
                        machineRepository.saveAndFlush(machine);

                    } catch (InterruptedException e) {
                        wrapper.value = "Interrupted in waiting";
                    }catch (ObjectOptimisticLockingFailureException exception) {
                        wrapper.value = "Machine already in process of starting";
                    }

                }, cronTrigger);

                if (!wrapper.value.equals("")){
                   return wrapper.value;
                }
                return "";

            }else{
                try {
                    Thread.sleep(10000 + random.nextInt(4000));
                    long millis=System.currentTimeMillis();
                    Date date = new Date(millis);
                    date.setHours(LocalDateTime.now().getHour());
                    date.setMinutes(LocalDateTime.now().getMinute());

                    machine.setStatus(Status.RUNNING);
                    machine.setDate(date);
                    machineRepository.saveAndFlush(machine);

                } catch (InterruptedException e) {
                   return "Interrupted in waiting";
                }catch (ObjectOptimisticLockingFailureException exception) {
                    return "Machine already in process of starting";
                }
                return "";
            }

    }

    public String stopMachine(Machine machine, ActionForm actionForm){

        Random random = new Random();

        if (actionForm.getDate()!=null) {

            Date date = actionForm.getDate();

            CronTrigger cronTrigger = new CronTrigger(date.getMinutes() + " " + date.getHours() + " " + date.getDate() + " " + date.getMonth() + " " + date.getDay() + " "  + date.getYear());

            //<Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week>
            // <Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week> <Year>

            var wrapper = new Object(){ String value = ""; };
            this.taskScheduler.schedule(() -> {

                try {
                    Thread.sleep(10000 + random.nextInt(4000));
                    machine.setStatus(Status.STOPPED);
                    machine.setDate(null);
                    machineRepository.saveAndFlush(machine);

                } catch (InterruptedException e) {
                    wrapper.value = "Interrupted in waiting";
                }catch (ObjectOptimisticLockingFailureException exception) {
                    wrapper.value = "Machine already in process of stopping";
                }

            }, cronTrigger);

            if (!wrapper.value.equals("")){
                return wrapper.value;
            }
            return "";

        }else{
            try {
                Thread.sleep(10000 + random.nextInt(4000));

                machine.setStatus(Status.STOPPED);
                machine.setDate(null);
                machineRepository.saveAndFlush(machine);

            } catch (InterruptedException e) {
                return "Interrupted in waiting";
            }catch (ObjectOptimisticLockingFailureException exception) {
                return "Machine already in process of stopping";
            }
            return "";
        }

    }

    public String restartMachine(Machine machine, ActionForm actionForm){

        Random random = new Random();
        int time = random.nextInt(4000);

        if (actionForm.getDate()!=null) {

            Date date = actionForm.getDate();

            CronTrigger cronTrigger = new CronTrigger(date.getMinutes() + " " + date.getHours() + " " + date.getDate() + " " + date.getMonth() + " " + date.getDay() + " "  + date.getYear());

            //<Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week>
            // <Minute> <Hour> <Day_of_the_Month> <Month_of_the_Year> <Day_of_the_Week> <Year>

            var wrapper = new Object(){ String value = ""; };
            this.taskScheduler.schedule(() -> {

                try {
                    Thread.sleep(5000 + time/2);
                    machine.setStatus(Status.STOPPED);
                    machine.setDate(actionForm.getDate());
                    machineRepository.saveAndFlush(machine);

                    Thread.sleep(5000 + time/2);
                    machine.setStatus(Status.RUNNING);
                    machine.setDate(actionForm.getDate());
                    machineRepository.saveAndFlush(machine);

                } catch (InterruptedException e) {
                    wrapper.value = "Interrupted in waiting";
                }catch (ObjectOptimisticLockingFailureException exception) {
                    wrapper.value = "Machine already in process of restarting";
                }

            }, cronTrigger);

            if (!wrapper.value.equals("")){
                return wrapper.value;
            }
            return "";

        }else{
            try {

                Thread.sleep(5000 + time/2);
                machine.setStatus(Status.STOPPED);
                machine.setDate(null);
                machineRepository.saveAndFlush(machine);

                Thread.sleep(5000 + time/2);
                long millis=System.currentTimeMillis();
                Date date = new Date(millis);
                date.setHours(LocalDateTime.now().getHour());
                date.setMinutes(LocalDateTime.now().getMinute());
                machine.setStatus(Status.RUNNING);
                machine.setDate(date);
                machineRepository.saveAndFlush(machine);

            } catch (InterruptedException e) {
                return "Interrupted in waiting";
            }catch (ObjectOptimisticLockingFailureException exception) {
                return "Machine already in process of restarting";
            }
            return "";
        }

    }
    private Date getToday(){
        long millis=System.currentTimeMillis();
        Date date = new Date(millis);
        date.setHours(LocalDateTime.now().getHour());
        date.setMinutes(LocalDateTime.now().getMinute());
        return date;
    }
}
