package raf.rs.NWP3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import raf.rs.NWP3.model.Machine;
import raf.rs.NWP3.model.Status;
import raf.rs.NWP3.model.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface MachineRepository  extends JpaRepository<Machine, Long> {

    public Optional<List<Machine>> findByCreatedByAndActiveTrue(User user);

    public Optional<List<Machine>> findByCreatedByAndNameContainingAndActiveTrue(User user,String  name);

    public Optional<List<Machine>> findByCreatedByAndDateBetweenAndActiveTrue(User user,Date dateFrom, Date dateTo);

    public Optional<List<Machine>> findByCreatedByAndStatusAndActiveTrue(User user,Status  status);

    public Optional<List<Machine>> findByCreatedByAndNameContainingAndStatusAndActiveTrue(User user,String  name, Status status);

    public Optional<List<Machine>> findByCreatedByAndNameContainingAndStatusAndDateBetweenAndActiveTrue(User user,String  name, Status status, Date dateFrom, Date dateTo);

    public Optional<List<Machine>> findByCreatedByAndStatusAndDateBetweenAndActiveTrue(User user,Status status, Date dateFrom, Date dateTo);

    public Optional<List<Machine>> findByCreatedByAndNameContainingAndDateBetweenAndActiveTrue(User user,String  name, Date dateFrom, Date dateTo);

    @Query("update Machine m set m.status = :status where m.id = :id")
    @Modifying
    @Transactional
    public void changeStatus(@Param("status") Status status, @Param("id") Long id);


    @Query("update Machine m set m.date = :date where m.id = :id")
    @Modifying
    @Transactional
    public void setDate(@Param("date") Date date, @Param("id") Long id);


}
