package raf.rs.NWP3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import raf.rs.NWP3.model.ErrorMessages;
import raf.rs.NWP3.model.Machine;
import raf.rs.NWP3.model.User;

import java.util.List;
import java.util.Optional;

public interface ErrorMessageRepository  extends JpaRepository<ErrorMessages, Long> {

    public Optional<List<ErrorMessages>> findByUser(User user);

}
