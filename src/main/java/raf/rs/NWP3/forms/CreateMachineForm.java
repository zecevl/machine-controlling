package raf.rs.NWP3.forms;

import lombok.Data;

@Data
public class CreateMachineForm {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
