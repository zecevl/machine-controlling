package raf.rs.NWP3.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import raf.rs.NWP3.model.ErrorMessages;
import raf.rs.NWP3.model.Errors;
import raf.rs.NWP3.model.User;
import raf.rs.NWP3.model.Users;
import raf.rs.NWP3.repositories.ErrorMessageRepository;
import raf.rs.NWP3.services.UserService;
import raf.rs.NWP3.util.JwtUtil;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final JwtUtil jwtUtil;
    private final PasswordEncoder passwordEncoder;
    private final ErrorMessageRepository errorMessageRepository;


    @Autowired
    public UserController(UserService userService, JwtUtil jwtUtil, PasswordEncoder passwordEncoder, ErrorMessageRepository errorMessageRepository) {
        this.userService = userService;
        this.jwtUtil = jwtUtil;
        this.passwordEncoder = passwordEncoder;
        this.errorMessageRepository = errorMessageRepository;
    }

    @GetMapping(value = "/whoAmI", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> whoAmI(@RequestHeader("Authorization") String token){
        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        Optional<User> optionalUser = userService.findByEmail(user.getUsername());
        if(optionalUser.isPresent()) {
            return new ResponseEntity<>(optionalUser.get(), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/all",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Users> getAllUsers(@RequestHeader("Authorization") String token){
        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: user.getAuthorities()) {
            if (obj.getAuthority().equals("can_read"))
                return new ResponseEntity<>(new Users(userService.findAll()), HttpStatus.ACCEPTED);
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    };

    @GetMapping(value = "/errors",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Errors> getErrors(@RequestHeader("Authorization") String token){
        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        User userReal = userService.findByEmail(user.getUsername()).get();

        return new ResponseEntity<>(new Errors(errorMessageRepository.findByUser(userReal).get()), HttpStatus.ACCEPTED);

    };

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserById(@RequestHeader("Authorization") String token,@PathVariable long id){
        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: user.getAuthorities()) {
            if (obj.getAuthority().equals("can_read")) {

                Optional<User> optionalUser = userService.findById(id);
                if(optionalUser.isPresent()) {
                    return new ResponseEntity<>(optionalUser.get(), HttpStatus.ACCEPTED);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> createUser(@RequestHeader("Authorization") String token,@RequestBody User user){

        Optional<User> optionalUser = userService.findByEmail(user.getEmail());
        if(optionalUser.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        UserDetails userDetails = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: userDetails.getAuthorities()) {
            if (obj.getAuthority().equals("can_create")) {

                user.setPassword(passwordEncoder.encode(user.getPassword()));

                return new ResponseEntity<>(userService.save(user), HttpStatus.ACCEPTED);
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);



    }

    @PutMapping(value = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> updateUser(@RequestHeader("Authorization") String token,@RequestBody User user,@PathVariable long id){

        Optional<User> optionalUser_check = userService.findByEmail(user.getEmail());
        if(!optionalUser_check.isPresent()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        UserDetails userDetails = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: userDetails.getAuthorities()) {
            if (obj.getAuthority().equals("can_update")) {

                Optional<User> optionalUser = userService.findById(id);
                if(optionalUser.isPresent()) {
                    User myUser = optionalUser.get();
                    myUser.setName(user.getName());
                    myUser.setLastname(user.getLastname());
                    myUser.setPassword(passwordEncoder.encode(user.getPassword()));
                    myUser.setEmail(user.getEmail());
                    myUser.setCan_create_users(user.isCan_create_users());
                    myUser.setCan_delete_users(user.isCan_delete_users());
                    myUser.setCan_read_users(user.isCan_read_users());
                    myUser.setCan_update_users(user.isCan_update_users());
                    userService.save(myUser);
                    return new ResponseEntity<>(userService.save(myUser), HttpStatus.ACCEPTED);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }


            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<User> deleteUser(@RequestHeader("Authorization") String token, @PathVariable Long id) {

        UserDetails user = this.checkJwt(token);
        if (user == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        for (GrantedAuthority obj: user.getAuthorities()) {
            if (obj.getAuthority().equals("can_delete")) {

                Optional<User> optionalUser = userService.findById(id);
                if(optionalUser.isPresent()) {
                    userService.deleteById(optionalUser.get().getId());
                    return new ResponseEntity<>(optionalUser.get(), HttpStatus.ACCEPTED);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);

    }
    
    private UserDetails checkJwt(String token){
        String jwt = null;
        String username = null;

        if(token != null && token.startsWith("Bearer ")) {
            jwt = token.substring(7);
            username = jwtUtil.extractUsername(jwt);
        }

        if (username != null ) {

            UserDetails userDetails = this.userService.loadUserByUsername(username);

            if (jwtUtil.validateToken(jwt, userDetails)) {

                return userDetails;
            }

        }
        return null;
    }

}
