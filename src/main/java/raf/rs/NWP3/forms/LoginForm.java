package raf.rs.NWP3.forms;

import lombok.Data;

@Data
public class LoginForm {
    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
