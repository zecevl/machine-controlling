package raf.rs.NWP3.model;

import java.util.List;

public class Errors {

    private List<ErrorMessages> errors;

    public Errors(List<ErrorMessages> errors) {
        this.errors = errors;
    }

    public List<ErrorMessages> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorMessages> errors) {
        this.errors = errors;
    }
}
