package raf.rs.NWP3.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String email;

    private String password;

    private String name;

    private String lastname;

    @OneToMany(mappedBy = "createdBy")
    private transient List<Machine> machineList = new ArrayList<>();

    private boolean can_read_users;

    private boolean can_create_users;

    private boolean can_update_users;

    private boolean can_delete_users;

    private boolean can_search_machines;
    private boolean can_start_machines;
    private boolean can_stop_machines;
    private boolean can_restart_machines;
    private boolean can_create_machines;
    private boolean can_destroy_machines;

    public void setId(long id) {
        this.id = id;
    }

    public List<Machine> getMachineList() {
        return machineList;
    }

    public void setMachineList(List<Machine> machineList) {
        this.machineList = machineList;
    }

    public boolean isCan_search_machines() {
        return can_search_machines;
    }

    public void setCan_search_machines(boolean can_search_machines) {
        this.can_search_machines = can_search_machines;
    }

    public boolean isCan_start_machines() {
        return can_start_machines;
    }

    public void setCan_start_machines(boolean can_start_machines) {
        this.can_start_machines = can_start_machines;
    }

    public boolean isCan_stop_machines() {
        return can_stop_machines;
    }

    public void setCan_stop_machines(boolean can_stop_machines) {
        this.can_stop_machines = can_stop_machines;
    }

    public boolean isCan_restart_machines() {
        return can_restart_machines;
    }

    public void setCan_restart_machines(boolean can_restart_machines) {
        this.can_restart_machines = can_restart_machines;
    }

    public boolean isCan_create_machines() {
        return can_create_machines;
    }

    public void setCan_create_machines(boolean can_create_machines) {
        this.can_create_machines = can_create_machines;
    }

    public boolean isCan_destroy_machines() {
        return can_destroy_machines;
    }

    public void setCan_destroy_machines(boolean can_destroy_machines) {
        this.can_destroy_machines = can_destroy_machines;
    }

    public long getId() {
        return id;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public boolean isCan_read_users() {
        return can_read_users;
    }

    public void setCan_read_users(boolean can_read_users) {
        this.can_read_users = can_read_users;
    }

    public boolean isCan_create_users() {
        return can_create_users;
    }

    public void setCan_create_users(boolean can_create_users) {
        this.can_create_users = can_create_users;
    }

    public boolean isCan_update_users() {
        return can_update_users;
    }

    public void setCan_update_users(boolean can_update_users) {
        this.can_update_users = can_update_users;
    }

    public boolean isCan_delete_users() {
        return can_delete_users;
    }

    public void setCan_delete_users(boolean can_delete_users) {
        this.can_delete_users = can_delete_users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id == user.id &&
                email.equals(user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email);
    }
}
